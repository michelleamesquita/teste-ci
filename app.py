from flask import Flask,request

app= Flask(__name__)

@app.route('/',methods=['GET'])
def home():
  return "Hello World :)", 200

if __name__=='__main__':
  app.run(debug=False, host='0.0.0.0')
